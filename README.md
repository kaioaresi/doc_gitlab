# Gitlab-ci
> https://docs.gitlab.com/ee/ci/

---
## O que é CI/CD ?

**Continuous integration**
  - Integra o código e roda os testes com mais frequência
  - Fazer merge só se green

**Continuous deployment**
  - Está tudo verde, faz deploy para um servidor
  - Fazer deploys mais frequentes
  - Deploy é implantar uma versão do software em um servidor

## Pipeline
  - Fluxo, ao qual é realizar o CI e CD


![Alt gitlab flow](img/gitlab_workflow_example_11_9.png)


![Alt pipeline demo](img/pipeline-demo.png)

---

## Informação iniciais
> https://docs.gitlab.com/ee/ci/introduction/

- No gitlab-ci, o nome do arquivo do pipeline é `.gitlab-ci.yml`, o mesmo deve esta na raiz do projeto.
- O gitlab possui o `runner` que é seu executor em servidores externos.
- A sintaxe do gitlab é bem simples em `yml`



## Primeiro pipeline
> https://docs.gitlab.com/ee/ci/yaml/

- Criar um arquivo com nome `.gitlab-ci.yml` na raiz do projeto
- Após realizar um configuração é necessário realizar um push para o repositorio

1 - Criando Job

> A imagem padrão utilizar é `ruby`

```
setup:
  stage: setup
  script: echo "--> Executando setup"
```

2 - Add image local e global

```
image: maven:3-jdk-8 # image global

setup:
  image: ubuntu
  stage: setup
  script: echo "--> Executando setup"

test-unit:
  stage: tests
  script: echo "--> Executando test-unit"
```

3 - Ordenando `stage`

```
stages:
  - setup
  - test
  - build
  - approvals
  - deploy
  - staging
  - notification

image: maven:3-jdk-8 # image global

setup:
  image: ubuntu
  stage: setup
  script: echo "--> Executando setup"

test-unit:
  stage: test
  script: mvn test

test-audit:
  stage: test
  script: mvn test
```

4 - Depedencias

```
stages:
  - setup
  - test
  - build
  - approvals
  - deploy
  - staging
  - notification

image: maven:3-jdk-8 # image global

setup:
  image: ubuntu
  stage: setup
  script: echo "--> Executando setup"

test-unit:
  stage: test
  script: mvn test

test-audit:
  stage: test
  script: mvn test

build-img:
  stage: build
  dependencies:
    - test-unit
    - test-audit
  when: on_success
  image: docker:dind
  script:
    - docker info
```

5 - Variables

```
stages:
  - setup
  - test
  - build
  - approvals
  - deploy
  - staging
  - notification

image: maven:3-jdk-8 # image global

variables:
  MAVEN_EXTRA: "-X"

setup:
  image: ubuntu
  stage: setup
  script: echo "--> Executando setup"

test-unit:
  stage: test
  script: mvn $MAVEN_EXTRA test

test-audit:
  stage: test
  script: mvn $MAVEN_EXTRA test

build-img:
  stage: build
  dependencies:
    - test-unit
    - test-audit
  when: on_success
  image: docker:dind
  script:
    - docker info
```

6 - Executando apenas

```
stages:
  - setup
  - test
  - build
  - approvals
  - deploy
  - staging
  - notification

image: maven:3-jdk-8 # image global

variables:
  MAVEN_EXTRA: "-X"

setup:
  image: ubuntu
  stage: setup
  script: echo "--> Executando setup"

test-unit:
  stage: test
  script: mvn $MAVEN_EXTRA test

test-audit:
  stage: test
  script: mvn $MAVEN_EXTRA test

build-img:
  stage: build
  dependencies:
    - test-unit
    - test-audit
  when: on_success
  image: docker:dind
  script:
    - docker info
  only:
    changes:
      - java_maven_springboot/*
      - '*.rb'
      - '*.py'

deploy-qa:
  stage: deploy
  image: alpine
  script:
    - echo "Deploy qa"
  only:
    refs:
      - qa

deploy-prod:
  stage: deploy
  image: alpine
  when: manual
  script:
    - echo "Deploy prod"
  only:
    refs:
      - master
```

7 - Gitlab-runner

```
stages:
  - setup
  - test
  - build
  - approvals
  - deploy
  - staging
  - notification

image: maven:3-jdk-8 # image global

variables:
  MAVEN_EXTRA: "-X"

setup:
  image: ubuntu
  stage: setup
  script: echo "--> Executando setup"

test-unit:
  stage: test
  script: mvn $MAVEN_EXTRA test

test-audit:
  stage: test
  script: mvn $MAVEN_EXTRA test

build-img:
  stage: build
  dependencies:
    - test-unit
    - test-audit
  when: on_success
  image: docker:dind
  script:
    - docker info
  only:
    changes:
      - java_maven_springboot/*
      - '*.rb'
      - '*.py'

deploy-qa:
  stage: deploy
  image: alpine
  script:
    - echo "Deploy qa"
  only:
    refs:
      - qa
  tags:
    - runner-qa

deploy-prod:
  stage: deploy
  image: alpine
  when: manual
  script:
    - echo "Deploy prod"
  only:
    refs:
      - master
  tags:
    - runner-prod
```

8 - Casos de falha e sucesso

```
stages:
  - setup
  - test
  - build
  - approvals
  - deploy
  - notification

image: maven:3-jdk-8 # image global

variables:
  MAVEN_EXTRA: "-X"

setup:
  image: ubuntu
  stage: setup
  script: echo "--> Executando setup"

test-unit:
  stage: test
  script: mvn $MAVEN_EXTRA test

test-audit:
  stage: test
  script: mvn $MAVEN_EXTRA test

build-img:
  stage: build
  dependencies:
    - test-unit
    - test-audit
  when: on_success
  image: docker:dind
  script:
    - docker info
  only:
    changes:
      - java_maven_springboot/*
      - '*.rb'
      - '*.py'

deploy-qa:
  stage: deploy
  image: alpine
  script:
    - echo "Deploy qa"
  only:
    refs:
      - qa
  tags:
    - runner-qa

deploy-prod:
  stage: deploy
  image: alpine
  when: manual
  script:
    - echo "Deploy prod"
  only:
    refs:
      - master
  tags:
    - runner-prod

notification-sucesso:
  stage: notification
  when: on_success
  script:
    - echo "tudo ok."

notification-fail:
  stage: notification
  when: on_failure
  script:
    - echo "ocorreu algum erro."
```



---

```
image: docker:stable # aqui estamos definindo uma image ao qual nosso pipeline será executado, essa img é será utilizada em todos job que não possuirem uma img definida.

services:
  - docker:dind # aqui estamos executando o docker in docker, partindo deste momento poderemos executar comandos docker.

before_script:
  - docker info

job1:
  stage: build
  script:
    - docker container run hello-world
```

```
job1:
  stage: build # em que passo nosso job está
  image: docker:stable # aqui estamos definindo uma image ao qual nosso pipeline será executado, essa img é será utilizada em todos job que não possuirem uma img definida.
  services:
    - docker:dind # aqui estamos executando o docker in docker, partindo deste momento poderemos executar comandos docker.
  before_script:
    - docker info
  script:
    - docker container run hello-world
```

## Dependências de jobs

Agora vamos criar, dois jobs (job1 e job2), o job2 só será executando quando o job1 for executado com sucesso, mas para isso precisamos fazer uma pequena alteração nos stage, para se adeguar a nossa necessidade.

> Informação importante, não é possível ter uma Dependências de um job do mesmo, stage, por esse motivo estamos adptando o `stages`.

```
image: docker:stable

stages:
  - pre-build
  - build

services:
  - docker:dind

before_script:
  - docker info

job1:
  stage: pre-build
  script:
    - docker container run hello-world

job2:
  stage: build
  script:
    - docker container run hello-world
  dependencies:
    - job1
```

## Runner

Neste [link](https://docs.gitlab.com/runner/install/), você pode ver algumas forma de se implementar o runner em seu ambiente local, para realizar um deploy por exemplo, no meu caso vou executar no meu host mesmo para fim de test.

Aqui estamos instalando o runner em um host.

```
docker run -d --name gitlab-runner --restart always --mount type=bind,src=/tmp/gitlab-runner/config,target=/etc/gitlab-runner --mount type=bind,src=/var/run/docker.sock,target=/var/run/docker.sock:ro gitlab/gitlab-runner:latest
```

Agora temos que realizar um sinc deste runner com nosso gitlab, pois ele é nosso executor

1 - Na interface grafica, clicando em `Settings` >> `CI/CD` >> `Runners` >> `Set up a specific Runner manually` copie o hash apresentado.
2 - Com o hash em mão, vamos acessar o container
3 - Dentro do container digite:
  ```
    gitlab-runner register
    # digite o utl do gitlab
    # o token
    # Descrição
    # image da app
  ```

Agora vamos configurar nossa pipeline.

```
image: docker:stable

stages:
  - pre-build
  - build

services:
  - docker:dind

job1:
  stage: pre-build
  before_script:
    - docker info
  script:
    - docker container run hello-world

job2:
  stage: build
  services:
    - docker:dind
  dependencies:
    - job1
  tags:
    - executor-ntb
  script:
    - ls -lh
```

---

## [Kubernetes runner](https://docs.gitlab.com/runner/install/kubernetes.html)

```
helm repo add gitlab https://charts.gitlab.io
helm install --name gitlab-runner gitlab/gitlab-runner -f <file>
helm upgrade gitlab-runner gitlab/gitlab-runner -f <file>
```

No pipeline precisamos realizar algumas configurações para que sejá possível realizarmos o deploy.

>> Esse procedimento é temporário, pois é um pouco rustico, mas funciona e não deixa o cluster vuneravel.


1 - Você deve criar um arquivo config, para que o mesmo seja utilizado para realizar deploy.
2 - Após a criação deste arquivo você pode 'echo $(cat ~/.kube/config | base64) | tr -d " "', copiar essa sair e salvar como um variables no github.
3 - Após realizar as configs acima, você poderá seguir com os passo abaixo:


```
variables:
  KUBECONFIG: /etc/deploy/config

setup-k8s:
  stage: setup-k8s
  before_script:
    - mkdir -p /etc/deploy
    - echo $KUBE_CONFIG |base64 -d > $KUBECONFIG   # config para ter a permissão utilizar o kubectl.
  script:
    - kubectl get pods
    - kubectl get pods -n kube-system
  tags:
    - k8s-lab
```

App de teste.

```
kubectl create deployment hello-app --image=gcr.io/google-samples/hello-app:1.0
kubectl expose deployment hello-app --port 8080 --target-port 8080
```

---

```
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: default
  namespace: gitlab
```


```
#deploy-staging:
#  stage: deploy
#  image: google/cloud-sdk
#  before_script:
#    - kubectl config set-cluster k8s --server=$KUBE_URL
#    - kubectl config set clusters.k8s.certificate-authority-data $KUBE_CA_DATA
#    - kubectl config set-credentials gitlab --token="$KUBE_USER_TOKEN"
#    - kubectl config set-context default --cluster=k8s --user=gitlab
#    - kubectl config use-context default
#  script:
#    - kubectl get pods
#  only:
#    refs:
#      - master

```




---


Outra opção

```
- USER_TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
- echo $USER_TOKEN
- CERTIFICATE_AUTHORITY_DATA=$(cat /var/run/secrets/kubernetes.io/serviceaccount/ca.crt | base64 -i -w0 -)
- echo $CERTIFICATE_AUTHORITY_DATA
- kubectl config set-cluster k8s --server="https://kubernetes.default.svc"
- kubectl config set clusters.k8s.certificate-authority-data ${CERTIFICATE_AUTHORITY_DATA}
- kubectl config set-credentials gitlab --token="${USER_TOKEN}"
- kubectl config set-context default --cluster=k8s --user=gitlab
- kubectl config use-context default
```
---

## Openshift (OKD)

```
sudo yum -y update
sudo yum install -y yum-utils device-mapper-persistent-data lvm2 wget vim curl git
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y  docker-ce docker-ce-cli containerd.io

sudo usermod -aG docker $USER
newgrp docker

sudo mkdir /etc/docker /etc/containers

sudo tee /etc/containers/registries.conf<<EOF
[registries.insecure]
registries = ['172.30.0.0/16']
EOF

sudo tee /etc/docker/daemon.json<<EOF
{
   "insecure-registries": [
     "172.30.0.0/16"
   ]
}
EOF

sudo systemctl daemon-reload
sudo systemctl restart docker

sudo systemctl enable docker

echo "net.ipv4.ip_forward = 1" | sudo tee -a /etc/sysctl.conf
sudo sysctl -p

DOCKER_BRIDGE=`docker network inspect -f "{{range .IPAM.Config }}{{ .Subnet }}{{end}}" bridge`
sudo firewall-cmd --permanent --new-zone dockerc
sudo firewall-cmd --permanent --zone dockerc --add-source $DOCKER_BRIDGE
sudo firewall-cmd --permanent --zone dockerc --add-port={80,443,8443}/tcp
sudo firewall-cmd --permanent --zone dockerc --add-port={53,8053}/udp
sudo firewall-cmd --reload

docker container run hello-world # validation docker

wget https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz
tar xvf openshift-origin-client-tools*.tar.gz
cd openshift-origin-client*/
sudo mv  oc kubectl  /usr/local/bin/

oc version # validation oc

# oc cluster up # up cluster local

oc cluster up --routing-suffix=lab-okd.lab --public-hostname=service-okd.lab


oc cluster down

vi ./openshift.local.clusterup/openshift-controller-manager/openshift-master.kubeconfig # replace local ip to public ip

oc login

user/pass: developer

# or as admin

oc login -u system:admin


oc new-project lab-gitlab

```


```
curl -L https://git.io/get_helm.sh | bash
helm init --client-only
helm plugin install https://github.com/rimusz/helm-tiller
```
